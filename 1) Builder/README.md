# Patrón de Diseño 

# BUILDER

* Felipe de Jesús Delgado Troncoso.
* 37180544

# ¿Por que se eligio este potrón?

1.- Es un patrón de diseño creacional que permite construir
objetos complejos paso a paso. Este patrón permite producir
diferentes tipos y representaciones de un objeto utilizando el
mismo código de construcción.

* Se decidio implementar este patrón para este problema porque es el más viable
por que estamos trabajando con clases muy parecida las cuales heredan de otras
donde sus cambio son minímos y por esa rázon no sería viable crear tantas clases.

* Este permite construir objetos paso a paso y así solo utilizaremos los pasos
necesarios para cada  tipo de figura.

* Incluso podemos hacer reutilización de código al crear varios productos similares.
