
/**
 * @author Felipe de Jesús Delgado Troncoso
 */
public class Forma {

    private double lado = 0.0;
    private double ancho = 0.0;
    private double area = 0.0;
    private String color = "";
    
    public void setLado(double lado){
        this.lado = lado;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public double getLado() {
        return lado;
    }

    public double getAncho() {
        return ancho;
    }

    public double getArea() {
        return area;
    }
    
    
}