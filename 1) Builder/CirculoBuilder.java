/**
 * 
 * @author Felipe de Jesús Delgado Troncoso
 */
public class CirculoBuilder extends FormaBuilder{

    @Override
    public void buildLado() {
        forma.setLado(0);
    }

    @Override
    public void buildAncho() {
        forma.setAncho(25);
    }

    @Override
    public void buildArea() {
        forma.setArea(80);
    }

    @Override
    public void buildColor() {
        forma.setColor("Blanco");
    }
}
