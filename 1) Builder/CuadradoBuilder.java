
/**
 * 
 * @author Felipe de Jesús Delgado Troncoso
 */
public class CuadradoBuilder extends FormaBuilder {

    @Override
    public void buildLado() {
        forma.setLado(55);
    }

    @Override
    public void buildAncho() {
        forma.setAncho(22);
    }

    @Override
    public void buildArea() {
        forma.setArea(41);
    }

    @Override
    public void buildColor() {
        forma.setColor("Azul");
    }

}
