/**
 * 
 * @author Felipe de Jesús Delgado Troncoso
 */
public class Geometria { // Clase Director
    private FormaBuilder formaBuilder;
    
    public void setFormaBuilder(FormaBuilder fb){
        formaBuilder = fb;
    }
    
    public Forma getForma(){
        return formaBuilder.getForma();
    }
    
    public void construirForma(){
        formaBuilder.crearNuevaForma();
        formaBuilder.buildLado();
        formaBuilder.buildAncho();
        formaBuilder.buildArea();
        formaBuilder.buildColor();
    }
}
