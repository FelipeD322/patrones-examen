/**
 * 
 * @author Felipe de Jesús Delgado Troncoso
 */
public class Principal {
    public static void main(String[] args) {
        Geometria geometria = new Geometria(); // Director
        FormaBuilder ciculoBuilder = new CirculoBuilder();
        FormaBuilder cuadradoBuilder = new CuadradoBuilder();
        
        geometria.setFormaBuilder(ciculoBuilder);
        geometria.construirForma();
        
        Forma forma = geometria.getForma();
        System.out.println("Crear figura nueva...");
        System.out.println("Figura de tipo Circulo \nDe color: "+forma.getColor());
        System.out.println("Con una area de: "+forma.getArea());
        
        geometria.setFormaBuilder(cuadradoBuilder);
        geometria.construirForma();
        
        forma = geometria.getForma();
        
        System.out.println("\n\nCrear figura nueva...");
        System.out.println("Figura de tipo Cuadrado \nDe color: "+forma.getColor());
        System.out.println("Con una area de: "+forma.getArea());
        System.out.println("Lado: "+forma.getLado());
        System.out.println("Ancho: "+forma.getAncho());
    }
}
