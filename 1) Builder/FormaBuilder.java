/**
 * @author Felipe de Jesús Delgado Troncoso
 */
public abstract class FormaBuilder { //ABSTRACT BUILDER}
    protected Forma forma;
    
    public Forma getForma(){ //regresar la forma
        return forma;
    }

    public void crearNuevaForma(){ // hacer una instancia de la forma
        forma = new Forma();
    }
    
    public abstract void buildLado();
    public abstract void buildAncho();
    public abstract void buildArea();
    public abstract void buildColor();
}
