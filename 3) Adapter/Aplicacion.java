
import java.util.Scanner;

/**
 * 
 * @author Felipe de Jesús Delgado Troncoso
 */
public class Aplicacion {

   private static  Scanner scanner = new Scanner(System.in);
   private static  Comida comida;
   
    public static void main(String[] args) {
        int opcion;
        do {            
            opcion = preguntarOpcion();
            switch(opcion){
                case 1:
                    comida = new Pizza();
                    prepararComida();
                    break;
                case 2:
                    comida = new PizzaEspecial();
                    prepararComida();
                    break;
                case 3:
                    comida = new PastelAdapter();
                    prepararComida();
                    break;
                case 4:
                    System.out.println("Fin del programa");
                    break;
                default:
                    System.out.println("La opción ingresada no es valida. Intente de nuevo");
            }
            System.out.println("\n\n");
        } while (opcion !=4);
    }
    private static int preguntarOpcion(){
        System.out.println(
            "MENÚ DE RESETAS\n"
            +"1.- Receta para cocinar Pizza.\n"
            +"2.- Receta para cocinar Pizza Especial. \n"
            +"3.- Receta para cocinar Pastel.\n"
            +"4.- Salir.\n"
            +"Seleccione opción del menú:"
                );
        return Integer.parseInt(scanner.nextLine());
    }
    
    private static void prepararComida(){
        comida.preparar();
        comida.cocinar();
        comida.servir();
    }
}
