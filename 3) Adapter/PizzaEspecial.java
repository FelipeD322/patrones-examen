/**
 * 
 * @author Felipe de Jesús Delgado Troncoso
 */
public class PizzaEspecial extends Comida{

    public PizzaEspecial(){
        super();
        System.out.println("Preparando pizza con ingredientes especiales y asignados...");
    }
    
    @Override
    public void preparar() {
        System.out.println("Preparar pizza con ingredientes exoticos y receta específica.");
    }

    @Override
    public void cocinar() {
        System.out.println("Cocinar la pizza por 38 min y sazonar un poco.");
    }

    @Override
    public void servir() {
        System.out.println("Servir la pizza al cliente con mucha salsa y catsup.");
    }

}
