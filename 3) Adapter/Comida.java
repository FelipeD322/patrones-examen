// Clase que toma como referencia la pizzeria
public abstract class Comida { //Métodos abtractos para poder adecuar
    abstract public void preparar();
    abstract public void cocinar();
    abstract public void servir();
}