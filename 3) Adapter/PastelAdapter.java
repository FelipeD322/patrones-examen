/**
 * 
 * @author Felipe de Jesús Delgado Troncoso
 */
public class PastelAdapter extends Comida{ // Para poder adaptar la clase.

    private Pastel paste; //La clase que se va a adapdar.
    
    public PastelAdapter(){
        super();
        System.out.println("Creando pastel adapter...");
        this.paste = new Pastel();
    }
    
    @Override
    public void preparar() {
        System.out.println("Prepara pastel adapter con harian y mucho chocolate.");
        this.paste.precalentar();
    }

    @Override
    public void cocinar() {
        System.out.println("Cocinar pastel adapter.");
        this.paste.prepararMolde();
        this.paste.ingredientes();
    }

    @Override
    public void servir() {
        System.out.println("Servir el pastel adapter listo al cliente");
        this.paste.adornar();
    }

}
