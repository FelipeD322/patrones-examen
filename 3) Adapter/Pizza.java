/**
 * 
 * @author Felipe de Jesús Delgado Troncoso
 */
public class Pizza extends Comida{

    public Pizza(){
        super();
        System.out.println("Preparando pizza común...");
    }
    
    @Override
    public void preparar() {
        System.out.println("Preparar pizza con ingredientes habituales.");
    }

    @Override
    public void cocinar() {
        System.out.println("Cocinar la pizza por 30 min.");
    }

    @Override
    public void servir() {
        System.out.println("Servir la pizza al cliente con un poco de salsa.");
    }

}
