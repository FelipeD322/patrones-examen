# Patrón de Diseño 

# ADAPTER

* Felipe de Jesús Delgado Troncoso.
* 37180544

# ¿Por que se eligio este potrón?

1.- Permite a objetos con interfaces incompatibles poder colaborar.

* En este caso se decidio utilizar este patrón ya que se tiene
una interfaz(Comida) que no es compatible con la clase Pastel.

* Es una forma de traducir código y así poder hacerlo adaptable a la otra clase.

* En este caso se implementa bien este tipo de diseño, ya que para una pizzeria
los empleados se tiene que adaptar y saber cómo preparar un pastel. 
Para eso se implementa la clase y se hace adaptable para poder saber la reseta y coninarlo. 

