/**
 * 
 * @author Felipe de Jesús Delgado Troncoso
 */
public class Pastel { //Esta clase no puede heredar directamente de Comida ya que en la pizzeria solo se hacen pizzas.
    
    public Pastel(){
        System.out.println("Creando pastel...");
    }
   
    public void precalentar(){
        System.out.println("Precantar el horno a 165°C");
    }
    
    public void prepararMolde(){
        System.out.println("Engrana y enharina un molde para el paste.");
    }
    
    public void ingredientes(){
        System.out.println("Incorpora los huevos y la vainilla");
    }
    
    public void adornar(){
        System.out.println("Poner chispas de chocolate en el pastel ya terminado");
    }
}
