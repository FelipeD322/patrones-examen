# Patrón de Diseño 

# OBSERVER

* Felipe de Jesús Delgado Troncoso.
* 37180544

# ¿Por que se eligio este potrón?

1.- Permite definir un mecanismo de suscripción para notificar
a múltiples objetos sobre cualquier evento que ocurra a un objeto
que esté siendo observdo.

* El motivo principal por el cual se decidió seleccionar este patrón de diseño
es por la metodología que tiene que notificar a los suscriptores de un observador
de cada cambio que se realiza.

* Se decidio aplicar ya que en este caso un objeto debes ser observado por otros
(periódico).

* Para este problema los observdores serían los suscriptores de la revista, a los cuales 
se les notificará de los eventos (Notificación para descargar revista en página web).


