public class Principal {
    public static void main(String[] args) {
        Periodico periodico = new Periodico();
        periodico.setNombre("***Periódico Imagen***");
        periodico.setDescripcion("Notificación a suscriptores de revista virtual.");
        
        Suscriptor cliente = new Suscriptor();
        cliente.setCodigo("CLRT0001");
        cliente.setNombre("Pedro Ignacio");
        cliente.setApellido("Perez");
        cliente.setEmail("pedroIgn@ejemplo.com");  
        
        periodico.addObserver(cliente);
        System.out.println(periodico.getNombre());
        System.out.println(periodico.getDescripcion());
        periodico.setNotificar("");
        System.out.println("Código de verificación: "+cliente.getCodigo());
        
        cliente.setCodigo("CRLL7022");
        cliente.setNombre("Maria");
        cliente.setApellido("Torres Aguillar");
        cliente.setEmail("marialabonita@ejemplo.com");
        
        System.out.println("\n\nNotificando a otro suscriptor...\n\n");
        
        periodico.addObserver(cliente);
        System.out.println(periodico.getNombre());
        System.out.println(periodico.getDescripcion());
        periodico.setNotificar("");
        System.out.println("Código de verificación: "+cliente.getCodigo());
    }
}
