import java.util.Date;
import java.util.Observable;
import java.util.Observer;


public class Suscriptor implements Observer { // Implementamos la interface
    private String codigo;
    private String nombre;
    private String apellido;
    private String email;
    private Date fechaNacimiento;
    
    public Suscriptor(){
        
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }    
       
    /**
     * Método que se ejecuta cuando se producen cambios
     * en la clase observada.
     */
    @Override
    public void update(Observable o, Object arg) {
        if (((String)arg).equals("Se a producido una notificación")) {
            System.out.println("La revista está disponible para descargar en nuestro sitio web.");
        }else if (((String)arg).equals("usuario notificado")) {
            System.out.println("El usuario: "+nombre+" "+apellido+" a sido notificado. \nCon E-mail: "+email+" \nAgregando suscriptor a la lista...");
        }
    }
    
}
