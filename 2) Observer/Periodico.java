import java.util.Observable; // Clase implementa sus métodos para definir una clase como un elemento observable.
import java.util.Observer; // interface que se implementa en la clase que tiene que observar los cambios de la otra clase.

/**
* Es la clase Observable. Los cambios que se produzcan serán
* observados inmediatamente por la clase Observer que se registren
* desde ésta
* @author Felipe Delgado
* @version 1.0
*/
public class Periodico extends Observable { // Clase de la cual se observan sus cambios 

	private String nombre;
	private String descripcion;
	private float precio;
	private String notificar;

	
//	Único observador
//	En el caso de que hubiera varios, podría guardarse
//	como coleccion

	private Observer observer;

	public Periodico(){}

	public String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public String getDescripcion(){
		return descripcion;
	}

	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}

	public float getPrecio(){
		return precio;
	}

	public void setPrecio(float precio){
		this.precio = precio;
	}

	public String getNotificar(){
		return notificar;
	}

	public void setNotificar(String notificar){
		this.notificar = notificar;
		notifyObservers();
	}

        /**
         * Método que permite añadir observadores de esta clase
         */
	@Override
	public void addObserver(Observer observer){
		this.observer = observer;
	}

        /**
         * Método que notifica los cambios a los observadores de esta clase
         */
	@Override 
	public void notifyObservers(){
            if (observer != null) {
		observer.update(this, "Se a producido una notificación");
                observer.update(this, "usuario notificado");
            }
            
	}
}